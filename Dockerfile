FROM python:3.5-slim

RUN apt-get update
RUN apt-get upgrade -y
RUN apt-get install -y libgeos-dev libgdal-dev
RUN apt-get install -y git-core gcc g++ make libffi-dev libssl-dev python3-dev build-essential libpq-dev libmemcached-dev curl libcairo2-dev
RUN apt-get install -y libtiff5-dev libjpeg-dev libfreetype6-dev webp zlib1g-dev pcre++-dev libpango1.0-dev

RUN apt-get install -y libev-dev
RUN apt-get install -y wget
RUN curl -sL https://deb.nodesource.com/setup_5.x | bash
RUN apt-get install -y nodejs
RUN apt-get autoremove -y
RUN pip install --upgrade pip
RUN pip install virtualenv

RUN npm install -g grunt@1.0.1

ENV LD_LIBRARY_PATH /usr/lib:/usr/lib/x86_64-linux-gnu:$LD_LIBRARY_PATH
